resource "google_container_cluster" "primary" {
  name               = "kube-jenkins"
  location           = "northamerica-northeast1-c"
  initial_node_count = 3
  network            = google_compute_network.test.name

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
    ]

    metadata = {
      disable-legacy-endpoints = "true"
    }

    labels = {
      project = "kube-jenkins"
      appname = "Jenkins-on-Kubernetes"
      owner = "David"
    }

    tags = ["gke-jenkins"]
  }

  timeouts {
    create = "30m"
    update = "40m"
  }

  addons_config {
    horizontal_pod_autoscaling {
      disabled = false
    }
    network_policy_config {
      disabled = false
    }
  }

  # master_authorized_networks_config {

  # }

  # ip_allocation_policy {
  #   cluster_ipv4_cidr_block  = "172.16.0.0/16"
  #   services_ipv4_cidr_block = "172.17.0.0/22"
  # }
  #
  # private_cluster_config {
  #   enable_private_nodes = true
  #   enable_private_endpoint = true
  #   master_ipv4_cidr_block = "172.18.0.0/28"
  # }


}

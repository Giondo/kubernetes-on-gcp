terraform {
  backend "gcs" {
    bucket  = "equifax-terraform-states"
    prefix  = "terraform/state/kubernetes-on-gcp"
  }
}

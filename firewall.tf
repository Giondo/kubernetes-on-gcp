resource "google_compute_firewall" "default" {
  name    = "fw-kube-jenkins"
  network = google_compute_network.test.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  target_tags = ["gke-jenkins"]
  source_ranges = ["0.0.0.0/0"]
}
